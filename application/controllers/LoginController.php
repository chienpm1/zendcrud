<?php

class LoginController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function loginAction()
    {
        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        $loginForm = new Application_Form_Login();
        $this->view->loginForm = $loginForm;
        $isLoginRequest = $request->isPost();

        if ($isLoginRequest) {
            $userCredential = $request->getPost();
            $isInValidUser = !$loginForm->isValid($userCredential);
            if ($isInValidUser) {
                return;
            }

            $adapter = new Zend_Auth_Adapter_DbTable(
                null,
                'user',
                'username',
                'password',
                null
            );
            $adapter->setIdentity($loginForm->getValue('username'));
            $adapter->setCredential($loginForm->getValue('password'));
            $auth = Zend_Auth::getInstance();
            $authUser = $auth->authenticate($adapter);

            if (!$authUser->isValid()) {
                $this->view->errorMessage = $authUser->getMessages();   
                return;
            }
            $storage = new Zend_Auth_Storage_Session();
            $storage->write($adapter->getResultRowObject());
            $this->redirect('profile/index');
        }
        
    }

    public function registerAction()
    {
        /** @var Zend_Controller_Request_Http $request  */
        $user = new Application_Model_DbTable_User();
        $request = $this->getRequest();
        $loginForm = new Application_Form_Login();
        $this->view->loginForm = $loginForm;
        $isRegisterUserRequest = $request->isPost();
        if($isRegisterUserRequest) {
            $userCredential = $request->getPost();
            $isInValidUser = !$loginForm->isValid($userCredential);
            if($isInValidUser) {
                return;
            }
            $newUser = $loginForm->getValues();
            $user->insert($newUser);
            $this->redirect('default/login/login');
        }
    }

    public function signoutAction()
    {
        Zend_Session::destroy();
        Zend_Auth::getInstance()->clearIdentity();
        $this->redirect('login/login');
    }


}






