<?php

use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class ProfileController extends Zend_Controller_Action
{

    public function init()
    {
        //$profiles = new Application_Model_DbTable_Profile();
    }

    public function indexAction()
    {
        $profiles = new Application_Model_DbTable_Profile();
        $view = $profiles->fetchAll();
        $this->view->profiles = $view;
    }

    public function getForm()
    {
        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('Profile name')
            ->setDescription('fill in ur name ')
            ->setRequired(true)
            ->addValidator('StringLength', false, array(10, 80))
            ->addFilters(array('StringTrim'));

        $address = new Zend_Form_Element_Text('address');
        $address->setLabel('Address')
            ->setDescription('Fill in ur address ')
            ->setRequired(true)
            ->addFilters(array('HtmlEntities'));
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setIgnore(true);
        $form = new Zend_Form();
        $form->addElements([$name, $address, $submit]);

        return $form;
    }

    public function insertAction()
    {
        /** @var Zend_Controller_Request_Http $request */
        $request                  = $this->getRequest();
        //$isRequestShowFormProfile = !$request->isPost();
        $isRequestPersitProfile   = $request->isPost();
        $profileForm              = new Application_Form_Profile();

        $this->view->profileForm  = $profileForm;

        if ($isRequestPersitProfile) {
            $profileTable     = new Application_Model_DbTable_Profile();
            $rawProfile       = $request->getPost();
            $isInvalidProfile = !$profileForm->isValid($rawProfile);

            if ($isInvalidProfile) {
                return;
            }

            $profile = $profileForm->getValues();
            $profileTable->insert($profile);
            return $this->redirect('default/profile/index');
        }
    }

    public function updateAction()
    {
        $request = $this->getRequest();
        $id = $request->getParam('id');
        $profiles = new Application_Model_DbTable_Profile();
        $profile = $profiles->fetchRow(['id' => $id]);
        $this->view->profile = $profile;
    }

    public function storeUpdateAction()
    {
        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        $id = $request->getParam('id');
        $profiles = new Application_Model_DbTable_Profile();
        if ($request->isPost()) {
            $name = $request->getPost('name');
            $address = $request->getPost('address');
            $profileUpdated = [
                'name' => $name,
                'address' => $address
            ];
            $profiles->update($profileUpdated, "id = $id");
            $this->redirect('default/profile/index');
        }
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        $id = $request->getParam('id');
        $profiles = new Application_Model_DbTable_Profile();
        if ($id > 0) {
            $profiles->delete("id = $id");
            $this->redirect('/profile/index');
        }
    }
}
