<?php

class Application_Form_Register extends Zend_Form
{

    public function init()
    {
        $username = new Zend_Form_Element_Text('username');
        $password = new Zend_Form_Element_Password('password');
        $passwordConfirm = new Zend_Form_Element_Password('passwordConfirm');

        $username->setRequired(true)
            ->addValidators([
                new Zend_Validate_Alpha(),
                new Zend_Validate_Db_NoRecordExists(['table' => 'user', 'field' => 'username'])
            ])
            ->addFilters([
                new Zend_Filter_StringTrim()
            ]);
        
        $password->setRequired(true)->addFilter(new Zend_Filter_StringTrim());
        $this->addElements([
            $username,
            $password
        ]);
    }


}

