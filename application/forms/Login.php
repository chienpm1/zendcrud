<?php

class Application_Form_Login extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        //$this->setMethod('post');

        $username = new Zend_Form_Element_Text('username');
        $password = new Zend_Form_Element_Password('password');

        $username->setRequired(true)
            ->addValidator(new Zend_Validate_Alpha())
            ->addFilters([
                new Zend_Filter_StringTrim()
            ]);
        
        $password->setRequired(true)->addFilter(new Zend_Filter_StringTrim());
        $this->addElements([
            $username,
            $password
        ]);
    }


}

