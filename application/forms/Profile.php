<?php

class Application_Form_Profile extends Zend_Form
{   
    public function init()
    {
        $name = new Zend_Form_Element_Text('name');
        $address = new Zend_Form_Element_Text('address');

        $name
            ->setRequired(true)
            ->addValidators([
                new Zend_Validate_Alpha(),
                new Zend_Validate_Db_NoRecordExists(['table' => 'profile','field' => 'name'])
            ])
            ->addFilters([
                new Zend_Filter_StringTrim(),
                new Zend_Filter_StringToUpper()
            ]);

        $address
            ->addValidators([
                (new Zend_Validate_Between(['min' => 3, 'max' => 5]))
                    ->setMessage(
                        "'%value%' khong  nam trong khoang '%min%' va '%max%'",Zend_Validate_Between::NOT_BETWEEN
                    )
            ])
            ->addFilters([
                new Zend_Filter_StringTrim(),
                new Zend_Filter_StringToUpper()
            ]);
        

        $this->addElements([
            $name,
            $address
        ]);
    }
}

