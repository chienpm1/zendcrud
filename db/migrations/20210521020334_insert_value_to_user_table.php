<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class InsertValueToUserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up(): void
    {
        $user = [
            'username' => 'chienpm',
            'password' => 'admin1234',
            'real_name' => 'phamminhchien'
        ];
        $table = $this->table('user');
        $table->insert($user);
        $table->saveData();
    }

    public function down()
    {
        $this->execute('DELETE FROM user');
    }
}
